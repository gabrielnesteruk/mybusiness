﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class LoggedUser
    {
        public User user { get; set; }
        public LoggedUser(User user)
        {
            this.user = user;
        }
    }
}
