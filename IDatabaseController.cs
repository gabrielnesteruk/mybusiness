﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    interface IDatabaseController
    {
        public void AddEntity(Entity entity);
        public void DeleteEntity(int id);
        public void UpdateEntity(Entity entity);
        public Entity GetEntity(int id);
        public List<Entity> GetEntityAsList(int id);
        public DataTable GetEntityAsTable(int id);
    }
}
