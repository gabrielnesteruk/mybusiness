﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class Product : Entity
    {
        public int id { get; set; }
        public string name { get; set; }
        public int price_per_unit { get; set; }
        public int companyID { get; set; }
    }
}
