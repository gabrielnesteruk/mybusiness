﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class User : Entity
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int companyID { get; set; }
    }
}
