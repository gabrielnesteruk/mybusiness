﻿using NStack;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class InvoiceView
    {
        private Label ClientLabel;
        private ComboBox ClientComboBox;
        private MainView mainView;
        private DataTable productsTable;
        private TableView productsTableView;
        private IDatabaseController clientsController;
        private List<string> clientsList;
        private int invoice_id = 0;

        public InvoiceView(FrameView frame, MainView mainView, IDatabaseController clientsController)
        {
            this.clientsController = clientsController;
            this.mainView = mainView;

            var productsTable = new DataTable();
            this.productsTable = productsTable;
            productsTable.Columns.Add("Nazwa produktu");
            productsTable.Columns.Add("Cena za sztukę");
            productsTable.Columns.Add("Ilość");
            productsTable.Columns.Add("Zniżka [%]");
            productsTable.Columns.Add("Całość");

            var clientLabel = new Label("Klient:")
            {
                Y = 1,
                X = 3
            };

            var clientComboBox = new ComboBox()
            {
                Y = Pos.Top(clientLabel) + 1,
                X = 3,
                Width = Dim.Percent(50),
                Height = Dim.Fill(2),
                ReadOnly = true
            };

            var productsAddButton = new Button("Dodaj produkt")
            {
                Y = Pos.Top(clientComboBox) + 2,
                X = Pos.Left(clientComboBox)
            };

            productsAddButton.Clicked += () =>
            {
                var addedProduct = new InvoiceAddProductView(new DatabaseProductController(), mainView.loggedUser);
                if (addedProduct.GetAddedProduct() != null)
                    AddProductToTable(addedProduct.GetAddedProduct());
            };

            var productsRemoveButton = new Button("Usuń produkt")
            {
                Y = Pos.Top(clientComboBox) + 2,
                X = Pos.Right(productsAddButton) + 1
            };

            productsRemoveButton.Clicked += () =>
            {
                RemoveProduct();
            };

            var productsLabel = new Label("Lista produktów:")
            {
                Y = Pos.Top(productsAddButton) + 2,
                X = Pos.Left(productsAddButton)
            };
            var productList = new TableView(productsTable)
            {
                X = Pos.Left(productsLabel),
                Y = Pos.Top(productsLabel) + 1,
                Width = Dim.Fill(),
                Height = Dim.Fill(),
                FullRowSelect = true
            };
            this.productsTableView = productList;

            var invoiceButton = new Button("Wygeneruj fakturę")
            {
                X = Pos.Left(productsLabel),
                Y = Pos.AnchorEnd(1),
            };
            invoiceButton.Clicked += () =>
            {
                CreateInvoice();
            };

            this.ClientLabel = clientLabel;
            this.ClientComboBox = clientComboBox;

            SetComboBoxSource();

            frame.Add(clientLabel, clientComboBox, productsAddButton, productsRemoveButton, productsLabel, productList, invoiceButton);
        }

        private void SetComboBoxSource()
        {
            var clientsList = new List<string>();
            var clients = clientsController.GetEntityAsTable(mainView.loggedUser.user.companyID);
            for(int i = 0; i < 5; i++)
                clients.Columns.RemoveAt(2);
            foreach (DataRow client in clients.Rows)
            {
                clientsList.Add(client["id"].ToString() + "- " + client["name"]);
            }

            this.clientsList = clientsList;
            this.ClientComboBox.SetSource(clientsList);
            this.ClientComboBox.SetNeedsDisplay();
        }

        private void AddProductToTable(InvoiceProduct product)
        {
            DataRow row = productsTable.NewRow();
            row["Nazwa produktu"] = product.name;
            row["Cena za sztukę"] = product.price_per_unit;
            row["Ilość"] = product.amount;
            row["Zniżka [%]"] = product.discount;
            row["Całość"] = product.total;
            productsTable.Rows.Add(row);
            productsTableView.Table = productsTable;
        }

        private void RemoveProduct()
        {
            if (productsTableView.SelectedRow >= 0)
            {
                productsTable.Rows.RemoveAt(productsTableView.SelectedRow);
                productsTableView.Table = productsTable;
            }
        }

        private void CreateInvoice()
        {
            if (ClientComboBox.SelectedItem < 0 || ClientComboBox.Text == ustring.Empty)
                return;
            
            int id = int.Parse(clientsList[ClientComboBox.SelectedItem].Substring(0, clientsList[ClientComboBox.SelectedItem].IndexOf('-')));
            var client = (Client)clientsController.GetEntity(id);
            List<string> clientInfo = new List<string>()
            {
                client.name,
                client.NIP,
                client.street,
                client.city,
                client.postCode,
                client.country
            };

            var company = Database.GetCompanyByID(mainView.loggedUser.user.companyID).First();
            List<string> ownerInfo = new List<string>()
            {
                company.name,
                company.NIP,
                company.street,
                company.city,
                company.postCode,
                company.country
            };

            decimal totalSum = 0;
            foreach(DataRow row in productsTable.Rows)
            {
                totalSum += decimal.Parse(row["Całość"].ToString());
            }
            List<TotalRow> total = new List<TotalRow>()
            {
                new TotalRow("VAT 23%", totalSum * 0.23M),
                new TotalRow("Suma", totalSum + totalSum * 0.23M)
            };
            var invoice = new InvoiceCreator()
            {
                InvoiceClient = clientInfo,
                InvoiceOwner = ownerInfo,
                Data = productsTable,
                Totals = total,
                invoice_id = this.invoice_id
            };

            var fileStream = new FileStream(@"..\\Invoices\\invoice_" + this.invoice_id + ".pdf", FileMode.OpenOrCreate);
            invoice.Save(fileStream);
            fileStream.Close();

            this.invoice_id++;
        }
    }
}
