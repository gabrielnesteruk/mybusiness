﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;
using NStack;

namespace myBusiness.UI
{
    class CompanyInfoView
    {
        public CompanyInfoView(FrameView frame, int loggedUserID)
        {
            var user = Database.GetUserByID(loggedUserID);
            var company = Database.GetCompanyByID(user[0].companyID);

            var companyName = new Label("Firma: " + company[0].name)
            {
                X = 4,
                Y = 1
            };
            frame.Add(companyName);

            var companyStreet = new Label("Adres: " + company[0].street + ",")
            {
                X = Pos.Left(companyName),
                Y = Pos.Top(companyName) + 2
            };
            frame.Add(companyStreet);

            var companyPostCode = new Label(company[0].postCode)
            {
                X = Pos.Right(companyStreet) + 1,
                Y = Pos.Top(companyStreet)
            };
            frame.Add(companyPostCode);

            var companyCity = new Label(company[0].city + ",")
            {
                X = Pos.Right(companyPostCode) + 1,
                Y = Pos.Top(companyPostCode)
            };
            frame.Add(companyCity);

            var companyCountry = new Label(company[0].country)
            {
                X = Pos.Right(companyCity) + 1,
                Y = Pos.Top(companyPostCode)
            };
            frame.Add(companyCountry);

            var companyNIP = new Label("NIP: " + company[0].NIP)
            {
                X = Pos.Left(companyName),
                Y = Pos.Top(companyCountry) + 2
            };
            frame.Add(companyNIP);

            var companyOwner = new Label("Właściciel: " + company[0].owner)
            {
                X = Pos.Left(companyName),
                Y = Pos.Top(companyNIP) + 2
            };
            frame.Add(companyOwner);

            var companyEmail = new Label("E-mail: " + company[0].email)
            {
                X = Pos.Left(companyName),
                Y = Pos.Top(companyOwner) + 2
            };
            frame.Add(companyEmail);

            var companyPhone = new Label("Telefon: " + company[0].phone)
            {
                X = Pos.Left(companyName),
                Y = Pos.Top(companyEmail) + 2
            };
            frame.Add(companyPhone);

            var loggedUser = new Label("Zalogowany użytkownik: " + user[0].username)
            {
                X = Pos.Left(companyName),
                Y = Pos.AnchorEnd(2)
            };
            frame.Add(loggedUser);
        }
    }
}
