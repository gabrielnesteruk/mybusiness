﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class SuccessView
    {
        private Label SuccessText { get; set; }
        private Button ExitButton { get; set; }
        private Dialog SuccessDialog { get; set; }

        public SuccessView(string message)
        {
            SuccessText = new Label(message)
            {
                X = Pos.Center(),
                Y = 1,
            };

            ExitButton = new Button("Ok")
            {
                X = Pos.Center(),
                Y = Pos.Bottom(SuccessText) + 1,
                Width = 10
            };

            ExitButton.Clicked += () =>
            {
                Application.RequestStop();
            };

            SuccessDialog = new Dialog("Sukces", 50, 10);
            SuccessDialog.ColorScheme = Colors.ColorSchemes["Dialog"];
            SuccessDialog.Add(SuccessText, ExitButton);
            Application.Run(SuccessDialog);
        }
    }
}
