﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class InvoiceAddProductView
    {
        private ComboBox ProductComboBox;
        private IDatabaseController productsController;
        private InvoiceProduct addedProduct;
        private List<string> productsList;
        private LoggedUser loggedUser { get; set; }
        private TextField PriceInput { get; set; }
        private TextField AmountInput { get; set; }
        private TextField DiscountInput { get; set; }
        public InvoiceAddProductView(IDatabaseController productsController, LoggedUser loggedUser)
        {
            this.productsController = productsController;
            this.loggedUser = loggedUser;

            var productLabel = new Label("Produkt:")
            {
                X = 2,
                Y = 2
            };

            var productComboBox = new ComboBox()
            {
                Y = Pos.Top(productLabel) + 1,
                X = Pos.Left(productLabel),
                Width = Dim.Percent(50),
                Height = Dim.Fill(2),
                ReadOnly = true
            };
            this.ProductComboBox = productComboBox;
            productComboBox.SelectedItemChanged += (e) =>
            {
                UpdateProudctPrice(productsList[productComboBox.SelectedItem]);
                productComboBox.SetNeedsDisplay();
            };

            var amountLabel = new Label("Ilość:")
            {
                X = Pos.Left(productComboBox),
                Y = Pos.Top(productComboBox) + 1
            };

            var amountInput = new TextField()
            {
                X = Pos.Left(amountLabel),
                Y = Pos.Top(amountLabel) + 1,
                Width = Dim.Percent(50)
            };
            this.AmountInput = amountInput;

            var priceLabel = new Label("Cena za sztuke:")
            {
                X = Pos.Left(amountInput),
                Y = Pos.Top(amountInput) + 1
            };

            var priceInput = new TextField()
            {
                X = Pos.Left(priceLabel),
                Y = Pos.Top(priceLabel) + 1,
                Width = Dim.Percent(50),
                ReadOnly = true
            };
            this.PriceInput = priceInput;

            var discountLabel = new Label("Zniżka [%]:")
            {
                X = Pos.Left(priceInput),
                Y = Pos.Top(priceInput) + 1
            };

            var discountInput = new TextField()
            {
                X = Pos.Left(discountLabel),
                Y = Pos.Top(discountLabel) + 1,
                Width = Dim.Percent(50)
            };
            this.DiscountInput = discountInput;

            PopulateProductsComboBox();

            var btnAdd = new Button("Dodaj");
            var btnCancel = new Button("Anuluj");
            btnAdd.Clicked += () => 
            {
                AddProduct();
                Application.RequestStop(); 
            };
            btnCancel.Clicked += () => 
            {
                addedProduct = null;
                Application.RequestStop(); 
            };

            var productDialog = new Dialog("Dodaj produkt", btnAdd, btnCancel)
            {
                Height = Dim.Percent(50),
                Width = Dim.Percent(50)
            };
            productDialog.Add(productLabel, productComboBox, amountLabel, amountInput, priceLabel, priceInput, discountLabel, discountInput);
            Application.Run(productDialog);
        }
        private void PopulateProductsComboBox()
        {
            var productsList = new List<string>();
            var products = productsController.GetEntityAsTable(loggedUser.user.companyID);
            products.Columns.RemoveAt(2);
            foreach (DataRow client in products.Rows)
            {
                productsList.Add(client["id"].ToString() + "- " + client["name"]);
            }
            this.productsList = productsList;
            ProductComboBox.SetSource(this.productsList);
            ProductComboBox.SetNeedsDisplay();
        }

        public InvoiceProduct GetAddedProduct() { return addedProduct; }

        private void UpdateProudctPrice(string productName)
        {
            if (productName != null && productName.Length > 2)
            {
                int id = Int32.Parse(productName.Substring(0, productName.IndexOf('-')));
                Product product = (Product)productsController.GetEntity(id);
                PriceInput.Text = product.price_per_unit.ToString();
            }
        }
        private void AddProduct()
        {
            bool isValidDiscount;
            bool isValidAmount;
            isValidDiscount = VerifyData.VerifyDiscount(DiscountInput.Text.ToString());
            isValidAmount = VerifyData.VerifyOnlyDigits(AmountInput.Text.ToString());

            if (isValidAmount && isValidDiscount)
            {
                string a = "abcd";
                string b = a.Substring(a.IndexOf('b'), a.Length - a.IndexOf('b'));

                addedProduct = new InvoiceProduct();
                int x = productsList[ProductComboBox.SelectedItem].IndexOf("-");
                int y = productsList[ProductComboBox.SelectedItem].Length;
                addedProduct.name = productsList[ProductComboBox.SelectedItem].Substring(x + 1, (y - x) - 1).Trim();
                addedProduct.price_per_unit = decimal.Parse(PriceInput.Text.ToString());
                addedProduct.amount = AmountInput.Text.ToString();
                addedProduct.discount = DiscountInput.Text.ToString();

                decimal discount_price = addedProduct.price_per_unit * (decimal.Parse(addedProduct.discount) / 100M);
                decimal updated_product_price = addedProduct.price_per_unit - discount_price;
                decimal total = updated_product_price * decimal.Parse(addedProduct.amount);

                addedProduct.total = total;
            }
            else
                new ErrorView("Nie udało się dodać produktu!");
        }
    }
}
