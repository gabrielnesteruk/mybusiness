﻿using Terminal.Gui;
using NStack;
using myBusiness.UI;
using System;

namespace myBusiness
{
	class App : Window
	{
		static public Window window { get; set; }
		static public Toplevel top { get; set; }

        static public void Main()
        {
            StartInit();

            FirstView firstView = new FirstView();
            window.Add(firstView.WelcomeText, firstView.LoginLabel, firstView.LoginField, firstView.PasswordLabel,
                        firstView.PasswordField, firstView.LoginButton, firstView.RegisterButton);
            firstView.InitEventsHandlers();

            Application.Run();
			Application.Shutdown();
			Console.WriteLine("Hello");
			Console.ReadLine();
        }

        private static void StartInit()
        {
			Application.Init();
			top = Application.Top;

			window = new Window("myBusiness")
			{
				X = 0,
				Y = 1,

				Width = Dim.Fill(),
				Height = Dim.Fill()
			};
			top.Add(window);

			var menu = new MenuBar(new MenuBarItem[] {
				new MenuBarItem ("Opcje", new MenuItem [] {
					new MenuItem ("Wyjdź", "", () => { if (Quit ()) top.Running = false; })
				})
			});
			top.Add(menu);
		}

		private static bool Quit()
		{
			var n = MessageBox.Query(50, 7, "Wyjście z programu", "Czy na pewno chcesz wyjść z programu?", "Tak", "Nie");
			return n == 0;
		}
	}
}