﻿using System;
using System.Collections.Generic;
using Terminal.Gui;


namespace myBusiness.UI
{
    class CreateUserView
    {

        private Dialog CreateUserDialog { get; set; }
        private Label UsernameLabel { get; set; }
        private TextField UsernameField { get; set; }
        private Label UserPasswordLabel { get; set; }
        private TextField UserPasswordField { get; set; }
        private Button CancelButton { get; set; }
        private Button RegisterButton { get; set; }

        private List<string> data;

        public CreateUserView(Dialog dialog, List<string> data)
        {
            this.data = data;
            CreateUserDialog = dialog;

            var frame = new FrameView("Tworzenie użytkownika")
            {
                X = Pos.Center(),
                Y = 3,
                Width = Dim.Percent(75),
                Height = 15
            };
            CreateUserDialog.Add(frame);

            var infoLabel = new Label("Podaj nazwę użytkownika i hasło, \nktórego będziesz używać do logowania się do aplikacji.")
            {
                X = 2,
                Y = 1
            };
            frame.Add(infoLabel);

            UsernameLabel = new Label("Nazwa użytkownika: ") { X = 2, Y = 4 };
            UsernameField = new TextField("") { X = Pos.Right(UsernameLabel), Y = Pos.Top(UsernameLabel) , Width = 30 };
            frame.Add(UsernameLabel, UsernameField);

            UserPasswordLabel = new Label("Hasło:") { X = Pos.Left(UsernameLabel), Y = Pos.Top(UsernameLabel) + 2};
            UserPasswordField = new TextField("") { X = Pos.Left(UsernameField), Y = Pos.Top(UsernameLabel) + 2, Width = 30, Secret = true };
            frame.Add(UserPasswordLabel, UserPasswordField);

            RegisterButton = new Button("Zarejestruj")
            {
                X = 2,
                Y = Pos.Top(UserPasswordField) + 4
            };

            CancelButton = new Button("Anuluj")
            {
                X = Pos.Right(RegisterButton),
                Y = Pos.Top(UserPasswordField) + 4
            };

            RegisterButton.Clicked += () =>
            {
                var isValidUsername = VerifyData.VerifyUsernameOrPassword(UsernameField.Text.ToString());
                var isValidPassword = VerifyData.VerifyUsernameOrPassword(UserPasswordField.Text.ToString());

                var output = Database.GetUserByUsername(UsernameField.Text.ToString());
                if (output.Count != 0)
                    isValidUsername = false;

                if(isValidUsername && isValidPassword)
                {
                    var company = new Company();
                    company.PopulateCompanyData(data);
                    if(Database.InsertCompany(company) == 0)
                    {
                        new ErrorView("Nie udało się utworzyć konta.");
                    }

                    User user = new User()
                    {
                        username = UsernameField.Text.ToString(),
                        password = BitConverter.ToString(Hashing.CalculateSHA256(UserPasswordField.Text.ToString())).Replace("-", ""),
                        companyID = Database.GetLastInsertedCompanyID()
                    };

                    if(Database.InsertUser(user) == 0)
                    {
                        new ErrorView("Nie udało się utworzyć konta.");
                    }
                    else
                    {
                        new SuccessView("Pomyślnie utworzono konto.");
                    }

                    Application.RequestStop();
                }
                else if(!isValidUsername)
                {
                    new ErrorView("Taki użytkownik już istnieje!");
                }
                else
                {
                    new ErrorView("Nazwa bądź hasło nie spełniają wymagań!");
                }
            };

            CancelButton.Clicked += () =>
            {
                Application.RequestStop();
            };

            frame.Add(RegisterButton, CancelButton);

            Application.Run(CreateUserDialog);
        }
    }
}
