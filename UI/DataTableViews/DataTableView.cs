﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    abstract class DataTableView
    {
        protected IDatabaseController databaseController;
        protected TableView entityTableView { get; set; }
        protected DataTable entityDataTable { get; set; }
        protected Button addEntityButton { get; set; }
        protected Button deleteEntityButton { get; set; }
        protected MainView mainView { get; set; }
        public DataTableView(IDatabaseController databaseController, MainView mainView, FrameView frame, string addText, string deleteText)
        {
            Setup(databaseController, mainView, frame, addText, deleteText);
        }
        public virtual void Setup(IDatabaseController databaseController, MainView mainView, FrameView frame, string addText, string deleteText)
        {
            this.databaseController = databaseController;
            this.mainView = mainView;
            this.entityDataTable = databaseController.GetEntityAsTable(mainView.loggedUser.user.companyID);

            entityTableView = new TableView(entityDataTable)
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Percent(80)
            };

            entityTableView.CellActivated += EditCurrentCell;

            entityTableView.FullRowSelect = true;

            addEntityButton = new Button(addText)
            {
                X = 1,
                Y = Pos.AnchorEnd(1)
            };
            deleteEntityButton = new Button(deleteText)
            {
                X = Pos.Right(addEntityButton) + 1,
                Y = Pos.AnchorEnd(1)
            };

            addEntityButton.Clicked += AddRow;
            deleteEntityButton.Clicked += DeleteRow;

            frame.Add(entityTableView, addEntityButton, deleteEntityButton);
        }

        public virtual void EditCurrentCell(TableView.CellActivatedEventArgs e)
        {
            if (e.Table == null || e.Col == 0)
                return;

            var oldValue = e.Table.Rows[e.Row][e.Col].ToString();
            bool okPressed = false;

            var ok = new Button("Ok", is_default: true);
            ok.Clicked += () => { okPressed = true; Application.RequestStop(); };
            var cancel = new Button("Anuluj");
            cancel.Clicked += () => { Application.RequestStop(); };
            var d = new Dialog("Wprowadź nową wartość", 60, 20, ok, cancel);

            var lbl = new Label()
            {
                X = 0,
                Y = 1,
                Text = e.Table.Columns[e.Col].ColumnName
            };

            var tf = new TextField()
            {
                Text = oldValue,
                X = 0,
                Y = 2,
                Width = Dim.Fill()
            };

            d.Add(lbl, tf);
            tf.SetFocus();

            Application.Run(d);

            if (okPressed)
            {
                try
                {
                    VerifyAndUpdateEditedCell(Int32.Parse(e.Table.Rows[e.Row][0].ToString()), e.Col, tf);
                    e.Table.Rows[e.Row][e.Col] = string.IsNullOrWhiteSpace(tf.Text.ToString()) ? DBNull.Value : (object)tf.Text;
                }
                catch (Exception ex)
                {
                    new ErrorView("Nie udało się zmodyfikować wartości.");
                }

                entityTableView.Update();
            }
        }

        public abstract void VerifyAndUpdateEditedCell(int rowID, int column, TextField textField);
        public abstract void AddRow();

        public virtual void DeleteRow()
        {
            if (entityTableView.SelectedRow < 0)
                return;

            bool okPressed = false;
            var rowID = entityDataTable.Rows[entityTableView.SelectedRow][0].ToString();

            var ok = new Button("Usuń", is_default: true);
            ok.Clicked += () => { okPressed = true; Application.RequestStop(); };
            var cancel = new Button("Anuluj");
            cancel.Clicked += () => { Application.RequestStop(); };

            var deleteDialog = new Dialog("Usuń rekord", 60, 20, ok, cancel);

            var lbl = new Label("Czy na pewno chcesz usunąć wiersz o ID: " + rowID + " ?")
            {
                X = 0,
                Y = 1
            };

            deleteDialog.Add(lbl);
            Application.Run(deleteDialog);

            if (okPressed)
            {
                databaseController.DeleteEntity(Int32.Parse(rowID));
                entityDataTable = databaseController.GetEntityAsTable(mainView.loggedUser.user.companyID);
                entityTableView.Table = entityDataTable;
            }
        }

    }
}
