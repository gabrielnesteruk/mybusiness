﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
	class EmployeesView : DataTableView
	{
		private Employee employee { get; set; }

		public EmployeesView(FrameView frame, MainView mainView, IDatabaseController databaseController)
			: base(databaseController, mainView, frame, "Dodaj pracownika", "Usuń pracownika") { }
		
		public override void AddRow()
        {

			var ok = new Button("Dodaj", is_default: true);
			var cancel = new Button("Anuluj");
			cancel.Clicked += () => { Application.RequestStop(); };

			var addDialog = new Dialog("Dodaj pracownika", 60, 20, ok, cancel);

			var nameLabel = new Label("Imię i nazwisko:")
			{
				X = 0,
				Y = 1,
			};

			var nameField = new TextField()
			{
				X = 0,
				Y = Pos.Top(nameLabel) + 1,
				Width = Dim.Fill()
			};

			var rankLabel = new Label("Stanowisko:")
			{
				X = 0,
				Y = Pos.Top(nameField) + 1,
			};

			var rankField = new TextField()
			{
				X = 0,
				Y = Pos.Top(rankLabel) + 1,
				Width = Dim.Fill()
			};

			var salaryLabel = new Label("Wynagrodzenie:")
			{
				X = 0,
				Y = Pos.Top(rankField) + 1,
			};

			var salaryField = new TextField()
			{
				X = 0,
				Y = Pos.Top(salaryLabel) + 1,
				Width = Dim.Fill()
			};

			var addressLabel = new Label("Adres zamieszkania:")
			{
				X = 0,
				Y = Pos.Top(salaryField) + 1,
			};

			var addressField = new TextField()
			{
				X = 0,
				Y = Pos.Top(addressLabel) + 1,
				Width = Dim.Fill()
			};

			var phoneLabel = new Label("Telefon:")
			{
				X = 0,
				Y = Pos.Top(addressField) + 1,
			};

			var phoneField = new TextField()
			{
				X = 0,
				Y = Pos.Top(phoneLabel) + 1,
				Width = Dim.Fill()
			};

			ok.Clicked += () =>
			{
				var isValidList = new List<bool>();
				isValidList.Add(VerifyData.VerifySimpleString(nameField.Text.ToString(), 50));
				isValidList.Add(VerifyData.VerifySimpleString(rankField.Text.ToString(), 50));
				isValidList.Add(VerifyData.VerifyOnlyDigits(salaryField.Text.ToString()));
				isValidList.Add(VerifyData.VerifySimpleString(addressField.Text.ToString(), 50));
				isValidList.Add(VerifyData.VerifyOnlyDigits(phoneField.Text.ToString()));

				foreach (var item in isValidList)
				{
					if (!item)
					{
						MessageBox.ErrorQuery("Błąd", "Niepoprawne dane", "Ok");
						return;
					}
				}

				var newEmployee = new Employee()
				{
					name = nameField.Text.ToString(),
					rank = rankField.Text.ToString(),
					salary = Int32.Parse(salaryField.Text.ToString()),
					address = addressField.Text.ToString(),
					phone = phoneField.Text.ToString(),
					companyID = mainView.loggedUser.user.companyID

				};

				databaseController.AddEntity(newEmployee);

				entityDataTable = databaseController.GetEntityAsTable(mainView.loggedUser.user.companyID);
				entityTableView.Table = entityDataTable;

				Application.RequestStop();
			};

			addDialog.Add(nameLabel, nameField, rankLabel, rankField, salaryLabel, salaryField, addressLabel, addressField, phoneLabel, phoneField);
			Application.Run(addDialog);
		}

        public override void VerifyAndUpdateEditedCell(int rowID, int column, TextField textField)
        {
            employee = (Employee)databaseController.GetEntity(rowID);
            if (column == 1)
                employee.name = textField.Text.ToString();
            else if (column == 2)
                employee.rank = textField.Text.ToString();
            else if (column == 3)
                employee.salary = Int32.Parse(textField.Text.ToString());
            else if (column == 4)
                employee.address = textField.Text.ToString();
            else if (column == 5)
                employee.phone = textField.Text.ToString();

			databaseController.UpdateEntity(employee);
		}
	}
}
