﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class ClientsView : DataTableView
    {
        public ClientsView(FrameView frame, MainView mainView, IDatabaseController databaseController)
            : base(databaseController, mainView, frame, "Dodaj klienta", "Usuń klienta") { }
        public override void AddRow()
        {
            var ok = new Button("Dodaj", is_default: true);
            var cancel = new Button("Anuluj");
            cancel.Clicked += () => { Application.RequestStop(); };

            var addDialog = new Dialog("Dodaj klienta", 60, 25, ok, cancel);

            var nameLabel = new Label("Nazwa: ")
            {
                X = 0,
                Y = 1,
            };

            var nameField = new TextField()
            {
                X = 0,
                Y = Pos.Top(nameLabel) + 1,
                Width = Dim.Fill()
            };

            var streetLabel = new Label("Ulica: ")
            {
                X = 0,
                Y = Pos.Top(nameField) + 2
            };

            var streetField = new TextField()
            {
                X = 0,
                Y = Pos.Top(streetLabel) + 1,
                Width = Dim.Fill()
            };

            var postCodeLabel = new Label("Kod pocztowy:")
            {
                X = 0,
                Y = Pos.Top(streetField) + 2
            };

            var postCodeField = new TextField()
            {
                X = 0,
                Y = Pos.Top(postCodeLabel) + 1,
                Width = Dim.Fill()
            };

            var cityLabel = new Label("Miasto:")
            {
                X = 0,
                Y = Pos.Top(postCodeField) + 2
            };

            var cityField = new TextField()
            {
                X = 0,
                Y = Pos.Top(cityLabel) + 1,
                Width = Dim.Fill()
            };

            var countryLabel = new Label("Kraj:")
            {
                X = 0,
                Y = Pos.Top(cityField) + 2
            };

            var countryField = new TextField()
            {
                X = 0,
                Y = Pos.Top(countryLabel) + 1,
                Width = Dim.Fill()
            };

            var NIPLabel = new Label("NIP:")
            {
                X = 0,
                Y = Pos.Top(countryField) + 2
            };

            var NIPField = new TextField()
            {
                X = 0,
                Y = Pos.Top(NIPLabel) + 1,
                Width = Dim.Fill()
            };

            ok.Clicked += () =>
            {
                var isValidList = new List<bool>();
                isValidList.Add(VerifyData.VerifySimpleString(nameField.Text.ToString(), 50));
                isValidList.Add(VerifyData.VerifySimpleString(streetField.Text.ToString(), 50));
                isValidList.Add(VerifyData.VerifyOnlyDigits(postCodeField.Text.ToString()));
                isValidList.Add(VerifyData.VerifySimpleString(cityField.Text.ToString(), 50));
                isValidList.Add(VerifyData.VerifySimpleString(countryField.Text.ToString(), 50));
                isValidList.Add(VerifyData.VerifyNIP(NIPField.Text.ToString()));

                foreach (var item in isValidList)
                {
                    if (!item)
                    {
                        MessageBox.ErrorQuery("Błąd", "Niepoprawne dane", "Ok");
                        return;
                    }
                }

                var newClient = new Client()
                {
                    name = nameField.Text.ToString(),
                    street = streetField.Text.ToString(),
                    postCode = postCodeField.Text.ToString(),
                    city = cityField.Text.ToString(),
                    country = countryField.Text.ToString(),
                    NIP = NIPField.Text.ToString(),
                    company_ID = mainView.loggedUser.user.companyID
                };

                databaseController.AddEntity(newClient);

                entityDataTable = databaseController.GetEntityAsTable(mainView.loggedUser.user.companyID);
                entityTableView.Table = entityDataTable;

                Application.RequestStop();
            };

            addDialog.Add(nameLabel, nameField, streetLabel, streetField, postCodeLabel, postCodeField, cityLabel, cityField, countryLabel, countryField,
                            NIPLabel, NIPField);
            Application.Run(addDialog);
        }

        public override void VerifyAndUpdateEditedCell(int rowID, int column, TextField textField)
        {
            var client = (Client)databaseController.GetEntity(rowID);
            switch (column)
            {
                case 1:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 50))
                            client.name = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 2:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 50))
                            client.street = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 3:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 50))
                            client.postCode = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 4:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 50))
                            client.city = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 5:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 50))
                            client.country = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 6:
                    {
                        if (VerifyData.VerifyNIP(textField.Text.ToString()))
                            client.NIP = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
            }
            databaseController.UpdateEntity(client);
        }
    }
}
