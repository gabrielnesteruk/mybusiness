﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;


namespace myBusiness.UI
{
    class ProductsView : DataTableView
    {
        public ProductsView(FrameView frame, MainView mainView, IDatabaseController databaseController)
            : base(databaseController, mainView, frame, "Dodaj produkt", "Usuń produkt") { }

        public override void AddRow()
        {
            var ok = new Button("Dodaj", is_default: true);
            var cancel = new Button("Anuluj");
            cancel.Clicked += () => { Application.RequestStop(); };

            var addDialog = new Dialog("Dodaj produkt", 60, 20, ok, cancel);
            var nameLabel = new Label("Nazwa produktu:")
            {
                X = 0,
                Y = 1
            };
            var nameField = new TextField()
            {
                X = 0,
                Y = Pos.Top(nameLabel) + 1,
                Width = Dim.Fill()
            };
            var priceLabel = new Label("Cena:")
            {
                X = 0,
                Y = Pos.Top(nameField) + 1,
            };

            var priceField = new TextField()
            {
                X = 0,
                Y = Pos.Top(priceLabel) + 1,
                Width = Dim.Fill()
            };

            ok.Clicked += () =>
            {
                var isValidList = new List<bool>();
                isValidList.Add(VerifyData.VerifySimpleString(nameField.Text.ToString(), 100));
                isValidList.Add(VerifyData.VerifyOnlyDigits(priceField.Text.ToString()));

                foreach (var item in isValidList)
                {
                    if (!item)
                    {
                        MessageBox.ErrorQuery("Błąd", "Niepoprawne dane", "Ok");
                        return;
                    }
                }

                var newProduct = new Product()
                {
                    name = nameField.Text.ToString(),
                    price_per_unit = Int32.Parse(priceField.Text.ToString()),
                    companyID = mainView.loggedUser.user.companyID

                };

                databaseController.AddEntity(newProduct);

                entityDataTable = databaseController.GetEntityAsTable(mainView.loggedUser.user.companyID);
                entityTableView.Table = entityDataTable;

                Application.RequestStop();
            };

            addDialog.Add(nameLabel, nameField, priceLabel, priceField);
            Application.Run(addDialog);
        }


        public override void VerifyAndUpdateEditedCell(int rowID, int column, TextField textField)
        {
            var product = (Product)databaseController.GetEntity(rowID);

            switch(column)
            {
                case 1:
                    {
                        if (VerifyData.VerifySimpleString(textField.Text.ToString(), 100))
                            product.name = textField.Text.ToString();
                        else
                            throw new DataException();
                        break;
                    }
                case 2:
                    {
                        if(VerifyData.VerifyOnlyDigits(textField.Text.ToString()))
                            product.price_per_unit = Int32.Parse(textField.Text.ToString());
                        else
                            throw new DataException();
                        break;
                    }
            }
            databaseController.UpdateEntity(product);
        }
    }
}
