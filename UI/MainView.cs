﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class MainView
    {
        public List<string> MenuList { get; set; }
        public ListView MenuListView { get; set; }
        public LoggedUser loggedUser { get; set; }
        public MainView(User user)
        {
            loggedUser = new LoggedUser(user);

            MenuList = new List<string>()
            {
                "O firmie",
                "Lista pracowników",
                "Lista produktów",
                "Lista klientów",
                "Faktury",
                "Wyloguj się"
            };

            var frameList = new FrameView()
            {
                X = 0,
                Y = 0,
                Width = Dim.Percent(20),
                Height = Dim.Fill(),
            };

            var frameInfo = new FrameView()
            {
                X = Pos.Right(frameList) + 1,
                Y = 0,
                Width = Dim.Percent(80),
                Height = Dim.Fill()
            };

            MenuListView = new ListView(MenuList)
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill(),
                SelectedItem = 0
            };
            frameList.Add(MenuListView);


            // ustawiamy focus na 1 element listy i wyswietlamy go
            frameList.Initialized += (o, e) =>
            {
                frameList.FocusFirst();
                new CompanyInfoView(frameInfo, loggedUser.user.id);
            };
            App.window.Add(frameList, frameInfo);

            // obsluga przelaczania elementow z listy
            MenuListView.SelectedItemChanged += (e) =>
            {
                frameInfo.RemoveAll();
                if (e.Item == 0)
                    new CompanyInfoView(frameInfo, loggedUser.user.id);
                else if(e.Item == 1)
                {
                    new EmployeesView(frameInfo, this, new DatabaseEmployeeController());
                }
                else if(e.Item == 2)
                {
                    new ProductsView(frameInfo, this, new DatabaseProductController());
                }
                else if(e.Item == 3)
                {
                    new ClientsView(frameInfo, this, new DatabaseClientController());
                }
                else if(e.Item == 4)
                {
                    new InvoiceView(frameInfo, this, new DatabaseClientController());
                }
            };

            MenuListView.OpenSelectedItem += (e) =>
            {
                if (e.Item == 5)
                    Logout();
            };
        }

        private void Logout()
        {
            var n = MessageBox.Query(50, 7, "Wylogowanie", "Czy na pewno chcesz się wylogować?", "Tak", "Nie");
            if(n == 0)
            {
                loggedUser = null;

                App.window.RemoveAll();
                Application.Refresh();
                FirstView firstView = new FirstView();
                App.window.Add(firstView.WelcomeText, firstView.LoginLabel, firstView.LoginField, firstView.PasswordLabel,
                            firstView.PasswordField, firstView.LoginButton, firstView.RegisterButton);
                firstView.InitEventsHandlers();
            }
        }
    }
}
