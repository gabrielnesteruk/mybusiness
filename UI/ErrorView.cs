﻿using NStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace myBusiness.UI
{
    class ErrorView
    {
        private Label ErrorText { get; set; }
        private Button ExitButton { get; set; }

        private Dialog ErrorDialog { get; set; }
        public ErrorView(string message)
        {
            ErrorText = new Label(message)
            {
                X = Pos.Center(),
                Y = 1,
            };

            ExitButton = new Button("Ok")
            {
                X = Pos.Center(),
                Y = Pos.Bottom(ErrorText) + 1,
                Width = 10
            };

            ExitButton.Clicked += () =>
            {
                Application.RequestStop();
            };

            ErrorDialog = new Dialog("Błąd")
            {
                Width = Dim.Percent(50),
                Height = Dim.Percent(30)
            };
            ErrorDialog.ColorScheme = Colors.ColorSchemes["Error"];
            ErrorDialog.Add(ErrorText, ExitButton);
            ErrorDialog.SetFocus();
            Application.Run(ErrorDialog);
        }
    }
}
