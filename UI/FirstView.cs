﻿using System;
using System.Collections.Generic;
using Terminal.Gui;

namespace myBusiness.UI
{
    class FirstView
    {
        public Label WelcomeText { get; private set; }
        public Label LoginLabel { get; private set; }
        public Label PasswordLabel { get; private set; }
        public TextField LoginField { get; private set; }
        public TextField PasswordField { get; private set; }
        public Button LoginButton { get; private set; }
        public Button RegisterButton { get; private set; }

        public FirstView()
        {
            WelcomeText = new Label("Witaj w myBusiness - aplikacji do zarządzania swoją firmą. \nZaloguj się do swojego konta za pomocą danych podanych podczas rejestracji.")
            {
                X = 1,
                Y = 3
            };

            LoginLabel = new Label("Login: ") { X = 3, Y = 7 };

            PasswordLabel = new Label("Hasło: ")
            {
                X = Pos.Left(LoginLabel),
                Y = Pos.Top(LoginLabel) + 2
            };

            LoginField = new TextField("")
            {
                X = Pos.Right(LoginLabel),
                Y = Pos.Top(LoginLabel),
                Width = 30
            };

            PasswordField = new TextField("")
            {
                Secret = true,
                X = Pos.Left(LoginField),
                Y = Pos.Top(LoginLabel) + 2,
                Width = Dim.Width(LoginField)
            };

            LoginButton = new Button("Zaloguj się")
            {
                X = Pos.Left(LoginField),
                Y = Pos.Top(PasswordField) + 3,
            };

            RegisterButton = new Button("Zarejestruj się")
            {
                X = Pos.Right(LoginButton) + 2,
                Y = Pos.Top(PasswordField) + 3,
            };
        }
        public void InitEventsHandlers()
        {
            RegisterButton.Clicked += () =>
            {
                RegistrationView registration = new RegistrationView();
            };

            LoginButton.Clicked += () =>
            {
                var loginCheck = VerifyData.VerifyUsernameOrPassword(LoginField.Text.ToString());
                var passwordCheck = VerifyData.VerifyUsernameOrPassword(PasswordField.Text.ToString());

                if (loginCheck && passwordCheck)
                {
                    var loggedUser = Database.GetUserByUsername(LoginField.Text.ToString());
                    if (loggedUser.Count == 0)
                    { 
                        new ErrorView("Konto nie istnieje");
                        return;
                    
                    }

                    var hashedPassword = BitConverter.ToString(Hashing.CalculateSHA256(PasswordField.Text.ToString())).Replace("-", "");
                    if (hashedPassword == loggedUser[0].password)
                    {
                        new SuccessView("Pomyślnie zalogowano");
                        App.window.RemoveAll();

                        new MainView(loggedUser[0]);
                    }
                    else
                        new ErrorView("Błędne hasło");
                }
                else
                    new ErrorView("Niepoprawne dane logowania");
            };
        }
    }
}
