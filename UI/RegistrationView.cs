﻿using System;
using Terminal.Gui;
using NStack;
using System.Collections.Generic;

namespace myBusiness.UI
{
    class RegistrationView
    {
        private Dialog RegistrationDialog { get; set; }
        private Label ComapnyNameLabel { get; set; }
        private TextField ComapnyNameField { get; set; }
        private Label ComapnyStreetLabel { get; set; }
        private TextField ComapnyStreetField { get; set; }
        private Label ComapnyPostcodeLabel { get; set; }
        private TextField ComapnyPostcodeField { get; set; }
        private Label ComapnyCityLabel { get; set; }
        private TextField ComapnyCityField { get; set; }
        private Label ComapnyCountryLabel { get; set; }
        private TextField ComapnyCountryField { get; set; }
        private Label ComapnyNIPLabel { get; set; }
        private TextField ComapnyNIPField { get; set; }
        private Label CompanyOwnerLabel { get; set; }
        private TextField CompanyOwnerField { get; set; }
        private Label CompanyEmailLabel { get; set; }
        private TextField CompanyEmailField { get; set; }
        private Label CompanyPhoneLabel { get; set; }
        private TextField CompanyPhoneField { get; set; }
        private Button CancelButton { get; set; }
        private Button NextButton { get; set; }

        public RegistrationView()
        {
            CancelButton = new Button("Anuluj");
            NextButton = new Button("Dalej");

            CancelButton.Clicked += () =>
            {
                Application.RequestStop();
            };

            NextButton.Clicked += () =>
            {
                // 1. Sprawdzenie poprawnosci danych
                var isValid = new List<Tuple<bool, string>>();
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifySimpleString(ComapnyNameField.Text.ToString().Trim(), 50), ComapnyNameLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifyOnlyLettersAndDigits(ComapnyStreetField.Text.ToString().Trim()), ComapnyStreetLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifyOnlyDigits(ComapnyPostcodeField.Text.ToString().Trim()), ComapnyPostcodeLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifySimpleString(ComapnyCityField.Text.ToString().Trim(), 50), ComapnyCityLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifySimpleString(ComapnyCountryField.Text.ToString().Trim(), 50), ComapnyCountryLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifyNIP(ComapnyNIPField.Text.ToString().Trim()), ComapnyNIPLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifySimpleString(CompanyOwnerField.Text.ToString().Trim(), 50), CompanyOwnerLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifyEmail(CompanyEmailField.Text.ToString().Trim()), CompanyEmailLabel.Text.ToString()));
                isValid.Add(new Tuple<bool, string>(VerifyData.VerifyOnlyDigits(CompanyPhoneField.Text.ToString().Trim()), CompanyPhoneLabel.Text.ToString()));

                bool canContinue = false;
                string failedField = String.Empty;
                foreach (var item in isValid)
                {
                    if(!item.Item1)
                    {
                        canContinue = false;
                        failedField = item.Item2;
                        break;
                    }
                    canContinue = true;
                }

                if (!canContinue)
                { 
                    new ErrorView($"Podano błędne dane! [{failedField.Substring(0, failedField.IndexOf(':')).Trim()}]");
                }
                else
                {
                    RegistrationDialog.RemoveAll();
                    Application.Refresh();
                    new CreateUserView(RegistrationDialog, 
                        new List<string>()
                        {
                            ComapnyNameField.Text.ToString(),
                            ComapnyStreetField.Text.ToString(),
                            ComapnyPostcodeField.Text.ToString(),
                            ComapnyCityField.Text.ToString(),
                            ComapnyCountryField.Text.ToString(),
                            ComapnyNIPField.Text.ToString(),
                            CompanyOwnerField.Text.ToString(),
                            CompanyEmailField.Text.ToString(),
                            CompanyPhoneField.Text.ToString()
                        });
                }
            };

            RegistrationDialog = new Dialog("Rejestracja konta", 0, 0, NextButton, CancelButton);

            ComapnyNameLabel = new Label("Firma:") { X = 2, Y = 2 };
            ComapnyNameField = new TextField("") { X = Pos.Right(ComapnyNameLabel) + 1, Y = Pos.Top(ComapnyNameLabel), Width = 50 };
            RegistrationDialog.Add(ComapnyNameLabel, ComapnyNameField);

            ComapnyStreetLabel = new Label("ul. :") { X = Pos.Left(ComapnyNameLabel), Y = Pos.Top(ComapnyNameField) + 2 };
            ComapnyStreetField = new TextField("") { X = Pos.Left(ComapnyNameField), Y = Pos.Top(ComapnyNameField) + 2, Width = 50 };
            RegistrationDialog.Add(ComapnyStreetLabel, ComapnyStreetField);

            ComapnyPostcodeLabel = new Label("Kod: ") { X = Pos.Left(ComapnyStreetLabel), Y = Pos.Top(ComapnyStreetField) + 2 };
            ComapnyPostcodeField = new TextField("") { X = Pos.Left(ComapnyStreetField), Y = Pos.Top(ComapnyStreetField) + 2, Width = 10 };
            RegistrationDialog.Add(ComapnyPostcodeField, ComapnyPostcodeLabel);

            ComapnyCityLabel = new Label("Miasto: ") { X = Pos.Right(ComapnyPostcodeField) + 2, Y = Pos.Top(ComapnyPostcodeField) };
            ComapnyCityField = new TextField("") { X = Pos.Right(ComapnyCityLabel), Y = Pos.Top(ComapnyPostcodeField), Width = 25 };
            RegistrationDialog.Add(ComapnyCityLabel, ComapnyCityField);

            ComapnyCountryLabel = new Label("Kraj: ") { X = Pos.Right(ComapnyCityField) + 2, Y = Pos.Top(ComapnyCityLabel) };
            ComapnyCountryField = new TextField("") { X = Pos.Right(ComapnyCountryLabel), Y = Pos.Top(ComapnyPostcodeField), Width = 25 };
            RegistrationDialog.Add(ComapnyCountryLabel, ComapnyCountryField);

            ComapnyNIPLabel = new Label("NIP: ") { X = Pos.Left(ComapnyPostcodeLabel), Y = Pos.Top(ComapnyPostcodeLabel) + 2 };
            ComapnyNIPField = new TextField("") { X = Pos.Left(ComapnyPostcodeField), Y = Pos.Top(ComapnyPostcodeField) + 2, Width = 25 };
            RegistrationDialog.Add(ComapnyNIPLabel, ComapnyNIPField);

            CompanyOwnerLabel = new Label("Właścicel: ") { X = Pos.Left(ComapnyNIPLabel), Y = Pos.Top(ComapnyNIPLabel) + 2 };
            CompanyOwnerField = new TextField("") { X = Pos.Right(CompanyOwnerLabel), Y = Pos.Top(ComapnyNIPField) + 2, Width = 25 };
            RegistrationDialog.Add(CompanyOwnerLabel, CompanyOwnerField);

            CompanyEmailLabel = new Label("E-mail: ") { X = Pos.Left(CompanyOwnerLabel), Y = Pos.Top(CompanyOwnerLabel) + 2 };
            CompanyEmailField = new TextField("") { X = Pos.Right(CompanyEmailLabel), Y = Pos.Top(CompanyOwnerField) + 2, Width = 40 };
            RegistrationDialog.Add(CompanyEmailLabel, CompanyEmailField);

            CompanyPhoneLabel = new Label("Telefon: ") { X = Pos.Left(CompanyEmailLabel), Y = Pos.Top(CompanyEmailLabel) + 2 };
            CompanyPhoneField = new TextField("") { X = Pos.Right(CompanyPhoneLabel), Y = Pos.Top(CompanyEmailField) + 2, Width = 25 };
            RegistrationDialog.Add(CompanyPhoneLabel, CompanyPhoneField);

            Application.Run(RegistrationDialog);
        }
    }
}
