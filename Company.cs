﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class Company : Entity
    {
        public int id { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string postCode { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string NIP { get; set; }
        public string owner { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

        public void PopulateCompanyData(List<string> data)
        {
            id =        0;
            name =      data[0];
            street =    data[1];
            postCode =  data[2];
            city =      data[3];
            country =   data[4];
            NIP =       data[5];
            owner =     data[6];
            email =     data[7];
            phone =     data[8];
        }
    }
}
