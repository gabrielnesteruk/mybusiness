﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class InvoiceProduct
    {
        public string name { get; set; }
        public decimal price_per_unit { get; set; }
        public string amount { get; set; }
        public string discount { get; set; }
        public decimal total { get; set; }
    }
}
