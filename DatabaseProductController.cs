﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class DatabaseProductController : IDatabaseController
    {
        public void AddEntity(Entity entity)
        {
            Product product = (Product)entity;
            Database.AddProduct(product.name, product.price_per_unit, product.companyID);
        }

        public void DeleteEntity(int id)
        {
            Database.DeleteProduct(id);
        }

        public Entity GetEntity(int id)
        {
            return Database.GetProductByID(id);
        }

        public List<Entity> GetEntityAsList(int id)
        {
            throw new NotImplementedException();
        }

        public DataTable GetEntityAsTable(int id)
        {
            return Database.GetProducts(id);
        }

        public void UpdateEntity(Entity entity)
        {
            Product product = (Product)entity;
            Database.UpdateProduct(product.name, product.price_per_unit, product.companyID, product.id);
        }
    }
}
