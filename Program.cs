﻿using System;
using System.Collections.Generic;
using Terminal.Gui;
using Dapper;
using System.Data;
using System.Linq;

namespace myBusiness
{
    class Program
    {
        public int Main(String[] args)
        {
            Application.Run<App>();
            //var db = new DataAccess();
            //db.InsertPerson("Gabriel", "123456789", "gabrielnesteruk@gmail.com", "Warnenczyka 32", "Poland", "15-136");
            //var people = new List<Person>();
            //people = db.GetPeople("Poland");
            //foreach (var person in people)
            //{
            //    Console.WriteLine(person.FullInfo);
            //}
            return 0;
        }
    }

    class Person
    {
        public int id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string postalZip { get; set; }


        public string FullInfo
        {
            get { return $"{name} {phone} {email} {address} {country} {postalZip}"; }
        }

    }

    class DataAccess
    {
        private string connection_string = "Server=.;Database=RandomBase;Trusted_Connection=True;";
        public List<Person> GetPeople(string country)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                var output = connection.Query<Person>($"select * from myTable where country='{ country }'").ToList();
                return output;
            }
        }

        public void InsertPerson(string name, string phone, string email, string address, string country, string postalZip)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                connection.Execute($"INSERT INTO dbo.myTable (name, phone, email, address, country, postalZip) " +
                                    $"VALUES ('{name}', '{phone}', '{email}', '{address}', '{country}', '{postalZip}')");
            }
            //INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
            //VALUES(1, 'Ramesh', 32, 'Ahmedabad', 2000.00);
        }
    }
}