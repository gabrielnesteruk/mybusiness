﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

namespace myBusiness
{
    class Database
    {
        private static string connection_string = ConfigurationManager.AppSettings.Get("connectionString");

        public static List<User> GetUserByID(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<User>($"select * from dbo.Users WHERE id = {id}").ToList();
            }
        }

        public static List<User> GetUserByUsername(string username)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<User>($"select * from dbo.Users WHERE username = '{username}'").ToList();
            }
        }

        public static int InsertUser(User user)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
               return connection.Execute($"INSERT INTO dbo.Users (username, password, companyID)" +
                                   $" VALUES('{user.username}', '{user.password}', '{user.companyID}')");
            }
        }

        public static List<Company> GetCompanies()
        {
            throw new NotImplementedException();
        }

        public static List<Company> GetCompanyByID(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<Company>($"select * from dbo.Companies WHERE id = {id}").ToList();
            }
        }

        public static int InsertCompany(Company company)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"INSERT INTO dbo.Companies (name, street, postCode, city, country, NIP, " +
                                    $"owner, email, phone) " +
                                    $"VALUES ('{company.name}', '{company.street}', '{company.postCode}', '{company.city}', " +
                                    $"'{company.country}', '{company.NIP}', '{company.owner}', '{company.email}', '{company.phone}')");
            }
        }

        public static int GetLastInsertedCompanyID()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                var output = connection.Query<int>($"select IDENT_CURRENT('Companies')").First();
                return output;
            }
        }

        public static DataTable GetEmployees(int companyID)
        {
            using (SqlConnection connection = new SqlConnection(connection_string))
            {
                SqlDataAdapter da = new SqlDataAdapter("select id, name, rank, salary, address, phone from dbo.Employees where companyID = " + companyID, connection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public static Employee GetEmployeeByID(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<Employee>($"select * from dbo.Employees WHERE id = {id}").ToList().First();
            }
        }

        public static int DeleteEmployee(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"DELETE FROM [dbo].[Employees] WHERE id = {id}");
            }
        }

        public static int AddEmployee(string name, string rank, int salary, string address, string phone, int companyID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"INSERT INTO [dbo].[Employees] ([name],[rank],[salary],[address],[phone],[companyID]) VALUES"
                                            + $"('{name}', '{rank}', {salary}, '{address}', '{phone}', {companyID})");
            }
        }

        public static int UpdateEmployee(string name, string rank, int salary, string address, string phone, int companyID, int employeeID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"UPDATE [dbo].[Employees] SET [name] = '{name}',[rank] = '{rank}',[salary] =  {salary},[address] = '{address}'," +
                    $"[phone] = '{phone}',[companyID] = {companyID} WHERE [id] = {employeeID}");
            }
        }

        public static DataTable GetProducts(int companyID)
        {
            using (SqlConnection connection = new SqlConnection(connection_string))
            {
                SqlDataAdapter da = new SqlDataAdapter("select id, name, price_per_unit from dbo.Products where companyID = " + companyID, connection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public static int AddProduct(string name, int price, int companyID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"INSERT INTO [dbo].[Products] ([name],[price_per_unit],[companyID]) VALUES"
                                            + $"('{name}', {price}, {companyID})");
            }
        }

        public static int DeleteProduct(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"DELETE FROM [dbo].[Products] WHERE id = {id}");
            }
        }

        public static int UpdateProduct(string name, int price, int companyID, int productID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"UPDATE [dbo].[Products] SET [name] = '{name}',[price_per_unit] = {price},[companyID] =  {companyID}" +
                                            $"WHERE [id] = {productID}");
            }
        }

        public static Product GetProductByID(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<Product>($"select * from dbo.Products WHERE id = {id}").ToList().First();
            }
        }

        public static DataTable GetClients(int companyID)
        {
            using (SqlConnection connection = new SqlConnection(connection_string))
            {
                SqlDataAdapter da = new SqlDataAdapter("select id, name, street, postCode, city, country, NIP from dbo.Clients where company_ID = " + companyID, connection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public static int AddClient(string name, string street, string postCode, string city, string country, string NIP, int companyID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"INSERT INTO [dbo].[Clients] ([name],[street],[postCode],[city],[country],[NIP],[company_ID]) VALUES"
                                            + $"('{name}', '{street}', '{postCode}', '{city}', '{country}', '{NIP}', {companyID})");
            }
        }

        public static int DeleteClient(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"DELETE FROM [dbo].[Clients] WHERE id = {id}");
            }
        }

        public static int UpdateClient(string name, string street, string postCode, string city, string country, string NIP, int companyID, int clientID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Execute($"UPDATE [dbo].[Clients] SET [name] = '{name}',[street] = '{street}',[postCode] = '{postCode}',[city] = '{city}',[country] = '{country}', [NIP] = '{NIP}',[company_ID] =  {companyID}" +
                                            $"WHERE [id] = {clientID}");
            }
        }

        public static Client GetClientByID(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connection_string))
            {
                return connection.Query<Client>($"select * from dbo.Clients WHERE id = {id}").ToList().First();
            }
        }
    }
}
