﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class DatabaseClientController : IDatabaseController
    {
        public void AddEntity(Entity entity)
        {
            Client client = (Client)entity;
            Database.AddClient(client.name, client.street, client.postCode, client.city, client.country, client.NIP, client.company_ID);
        }

        public void DeleteEntity(int id)
        {
            Database.DeleteClient(id);
        }

        public Entity GetEntity(int id)
        {
            return Database.GetClientByID(id);
        }

        public List<Entity> GetEntityAsList(int id)
        {
            throw new NotImplementedException();
        }

        public DataTable GetEntityAsTable(int id)
        {
            return Database.GetClients(id);
        }

        public void UpdateEntity(Entity entity)
        {
            Client client = (Client)entity;
            Database.UpdateClient(client.name, client.street, client.postCode, client.city, client.country, client.NIP, client.company_ID, client.id);
        }
    }
}
