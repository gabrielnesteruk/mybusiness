﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class Employee : Entity
    {
        public int id { get; set; }
        public string name { get; set; }
        public string rank { get; set; }
        public int salary { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public int companyID { get; set; }
    }
}
