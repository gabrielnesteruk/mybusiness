﻿using Aspose.Pdf;
using Aspose.Pdf.Text;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    public class TotalRow
    {
        public string Text;
        public decimal Value;

        public TotalRow(string text, decimal value)
        {
            Text = text;
            Value = value;
        }
    }

    class InvoiceCreator
    {
        private static readonly License Licence = new License();
        private Color _textColor, _backColor;
        private readonly Font _timeNewRomanFont;
        private readonly TextBuilder _builder;
        private readonly Page _pdfPage;
        private readonly Document _pdfDocument;
        public int invoice_id { get; set; }

        public List<string> InvoiceClient { get; set; }
        public List<string> InvoiceOwner { get; set; }
        public DataTable Data { get; set; }

        public List<TotalRow> Totals { get; set; }

        public string ForegroundColor
        {
            get { return _textColor.ToString(); }
            set { _textColor = Color.Parse(value); }
        }
        public string BackgroundColor
        {
            get { return _backColor.ToString(); }
            set { _backColor = Color.Parse(value); }
        }

        public InvoiceCreator()
        {
            _pdfDocument = new Document();
            _pdfDocument.PageInfo.Margin.Left = 36;
            _pdfDocument.PageInfo.Margin.Right = 36;
            _pdfPage = _pdfDocument.Pages.Add();
            _textColor = Color.Black;
            _backColor = Color.Transparent;
            _timeNewRomanFont = FontRepository.FindFont("Times New Roman");
            _builder = new TextBuilder(_pdfPage);
        }

        private void HeaderSection()
        {
            var lines = new TextFragment[3];
            lines[0] = new TextFragment($"FAKTURA #{invoice_id}");
            lines[0].TextState.FontSize = 20;
            lines[0].TextState.ForegroundColor = _textColor;
            lines[0].HorizontalAlignment = HorizontalAlignment.Center;
            lines[0].Margin = new MarginInfo(0, 45, 0, 0);

            _pdfPage.Paragraphs.Add(lines[0]);

            lines[1] = new TextFragment($"DATA WYSTAWIENIA: {DateTime.Today:MM/dd/yyyy}");
            lines[2] = new TextFragment($"DATA WYKONANIA: {DateTime.Today.AddDays(7):MM/dd/yyyy}");
            for (var i = 1; i < lines.Length; i++)
            {              
                lines[i].TextState.Font = _timeNewRomanFont;
                lines[i].TextState.FontSize = 12;
                lines[i].HorizontalAlignment = HorizontalAlignment.Right;

                _pdfPage.Paragraphs.Add(lines[i]);
            }
        }

        private void AddressSection()
        {
            var box = new FloatingBox(524, 120)
            {
                ColumnInfo =
                {
                    ColumnCount = 2,
                    ColumnWidths = "252 252"
                },
                Padding =
                {
                    Top = 20
                }
            };
            TextFragment fragment;

            InvoiceOwner.Insert(0, "NADAWCA:");
            foreach (var str in InvoiceOwner)
            {
                fragment = new TextFragment(str);
                fragment.TextState.Font = _timeNewRomanFont;
                fragment.TextState.FontSize = 12;
                box.Paragraphs.Add(fragment);
            }

            fragment = new TextFragment("NABYWCA:") { IsFirstParagraphInColumn = true };
            fragment.TextState.Font = _timeNewRomanFont;
            fragment.TextState.FontSize = 12;
            fragment.TextState.HorizontalAlignment = HorizontalAlignment.Right;
            box.Paragraphs.Add(fragment);

            foreach (var str in InvoiceClient)
            {
                fragment = new TextFragment(str);
                fragment.TextState.Font = _timeNewRomanFont;
                fragment.TextState.FontSize = 12;
                fragment.TextState.HorizontalAlignment = HorizontalAlignment.Right;
                box.Paragraphs.Add(fragment);
            }
            _pdfPage.Paragraphs.Add(box);
        }

        private void GridSection()
        {
            // Initializes a new instance of the Table
            Table table = new Table();

            table.ColumnWidths = "20 150 70 70 70 70";
            var marginInfo = new MarginInfo();
            marginInfo.Top = 60;
            table.Margin = marginInfo; 
            table.DefaultCellBorder = new BorderInfo(BorderSide.All, .5f, Color.FromRgb(System.Drawing.Color.Black));
            table.DefaultCellPadding = new MarginInfo(4.5, 4.5, 4.5, 4.5);
            table.HorizontalAlignment = HorizontalAlignment.Center;

            Row headerRow = table.Rows.Add();
            var cell = headerRow.Cells.Add("#");
            cell.Alignment = HorizontalAlignment.Center;
            headerRow.Cells.Add("Nazwa");
            headerRow.Cells.Add("Cena");
            headerRow.Cells.Add("Ilość");
            headerRow.Cells.Add("Zniżka [%]");
            headerRow.Cells.Add("Suma");

            foreach (Cell headerRowCell in headerRow.Cells)
            {
                headerRowCell.BackgroundColor = Color.LightGray;
            }

            int id = 0;
            foreach (DataRow productItem in Data.Rows)
            {
                var row = table.Rows.Add();
                cell = row.Cells.Add(id++.ToString());
                cell.Alignment = HorizontalAlignment.Center;
                row.Cells.Add(productItem["Nazwa produktu"].ToString());
                cell = row.Cells.Add(productItem["Cena za sztukę"].ToString());
                cell.Alignment = HorizontalAlignment.Right;
                cell = row.Cells.Add(productItem["Ilość"].ToString());
                cell.Alignment = HorizontalAlignment.Right;
                cell = row.Cells.Add(productItem["Zniżka [%]"].ToString());
                cell.Alignment = HorizontalAlignment.Right;
                cell = row.Cells.Add(productItem["Całość"].ToString());
                cell.Alignment = HorizontalAlignment.Right;
            }

            foreach (var totalRow in Totals)
            {
                var row = table.Rows.Add();
                var nameCell = row.Cells.Add(totalRow.Text);
                nameCell.ColSpan = 5;
                var textCell = row.Cells.Add(totalRow.Value.ToString("C2"));
                textCell.Alignment = HorizontalAlignment.Right;
            }

            _pdfPage.Paragraphs.Add(table);
        }

        public void Save(Stream stream)
        {
            HeaderSection();
            AddressSection();
            GridSection();

            _pdfDocument.Save(stream);
        }
    }
}
