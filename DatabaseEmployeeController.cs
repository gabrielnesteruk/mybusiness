﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class DatabaseEmployeeController : IDatabaseController
    {
        public void AddEntity(Entity entity)
        {
            Employee employee = (Employee)entity;
            Database.AddEmployee(employee.name, employee.rank, employee.salary, employee.address, employee.phone, employee.companyID);
        }

        public void DeleteEntity(int id)
        {
            Database.DeleteEmployee(id);
        }

        public Entity GetEntity(int id)
        {
            return Database.GetEmployeeByID(id);
        }

        public List<Entity> GetEntityAsList(int id)
        {
            throw new NotImplementedException();
        }

        public DataTable GetEntityAsTable(int id)
        {
            return Database.GetEmployees(id);
        }

        public void UpdateEntity(Entity entity)
        {
            Employee employee = (Employee)entity;
            Database.UpdateEmployee(employee.name, employee.rank, employee.salary, employee.address, employee.phone, employee.companyID, employee.id);
        }
    }
}
