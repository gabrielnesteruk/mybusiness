﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace myBusiness
{
    class VerifyData
    {
        private const int minCharacters = 5;
        private const int maxCharacters = 15;

        public static bool VerifySimpleString(string data, int max_size)
        {
            if (string.IsNullOrEmpty(data) || data.Length > max_size)
                return false;

            return true;
        }

        public static bool VerifyOnlyDigits(string data)
        {
            if (string.IsNullOrEmpty(data))
                return false;

            foreach (char c in data)
            {
                if (!Char.IsDigit(c))
                    return false;
            }

            return true;
        }

        public static bool VerifyEmail(string data)
        {
            if (string.IsNullOrEmpty(data))
                return false;

            bool isValid;
            try
            {
                MailAddress m = new MailAddress(data);
                isValid = true;
            }
            catch (FormatException fx)
            {
                isValid = false;
            }

            return isValid;
        }

        public static bool VerifyOnlyLettersAndDigits(string data)
        {

            if (string.IsNullOrEmpty(data))
                return false;

            if(!CheckLength(data))
                return false;

            foreach (char c in data)
            {
                if (!Char.IsLetterOrDigit(c) && !Char.IsWhiteSpace(c))
                    return false;
            }

            return true;
        }

        public static bool VerifyUsernameOrPassword(string data)
        {
            return CheckLength(data) && VerifyOnlyLettersAndDigits(data);
        }

        public static bool VerifyNIP(string data)
        {
            return data.Length == 10 && VerifyOnlyDigits(data);
        }

        private static bool CheckLength(string data)
        {
            return !(data.Length > maxCharacters || data.Length < minCharacters);
        }

        public static bool VerifyDiscount(string data)
        {
            if (VerifyOnlyDigits(data))
                return Int32.Parse(data) >= 0 && Int32.Parse(data) <= 100;
            return false;
        }
    }
}
