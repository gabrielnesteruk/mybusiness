﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBusiness
{
    class Client : Entity
    {
        public int id { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string postCode { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string NIP { get; set; }
        public int company_ID { get; set; }
    }
}
